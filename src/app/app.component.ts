import { Component, OnInit, ViewChild } from '@angular/core';
import { from } from 'rxjs';
import { Details } from './details';
import * as d3 from 'd3';
import * as d3Scale from 'd3';
import * as d3Shape from 'd3';
import * as d3Axis from 'd3';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { ForcastServiceService } from './forcast-service.service';
import { ForcastValues } from './ForcastValues';
import { Forcasting } from './Forcasting';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  csvRecords: any[] = [];
  jsondata;
  jsonObj:any[]=[];
  jsonObj2:any[]=[];
  n;
  week;
  maxdate;
  j;
  display;
  l;
  forcastDate;
  newforcast:any[]=[];
  test:any[]=[];
  header = false;
  public title = 'Line Chart';
  public today=new Date();
  public time= new Date().getTime;
 LocationValue:string;
 ProductValue:string;
 forcastvalues:any[];
 userforms=new Details(2);
 private margin = {top: 20, right: 20, bottom: 30, left: 50};
 private width: number;
 private height: number;
 private x: any;
 private y: any;
 n4=[];
 n5=[];
 private svg: any;
 n2:any[];
 private line: d3Shape.Line<[number, number]>; // this is line defination
 private line2: d3Shape.Line<[number, number]>;
 constructor(private forcast:ForcastServiceService,private ngxCsvParser: NgxCsvParser) {
  this.width = 1300 - this.margin.left - this.margin.right;
  this.height = 600 - this.margin.top - this.margin.bottom;
 }
  ngOnInit(): void {
    
  }


onSubmit()
{
  
this.forcast.enroll(this.userforms).subscribe(data =>
  {
    
    this.l=data.length;
    this.newforcast.push(this.jsonObj[this.jsonObj.length-1]);
    for(this.j=0,this.week=1;this.j<data.length;this.j++)
    {
      var forcast2=new ForcastValues();
      var forcasting= new Forcasting();
      forcasting.TotFactoredVolume=Math.round(data[this.j]).toString();
      forcast2.TotFactoredVolume=Math.round(data[this.j]).toString();
      var testDate = new Date("12-30-2019");
      var weekInMilliseconds = (7 * 24 * 60 * 60 * 1000)*this.week;

      testDate.setTime(testDate.getTime() + weekInMilliseconds);
      let d5=testDate;
      
  //  this.maxdate=new Date(testDate)
      console.log(d5,"d5date")
      // let date = JSON.stringify(d5)
      // date = date.slice(1,11);
      // date=date.split("-").reverse().join("-");
      const monthNames = ["Jan", "Feb", "March", "Apr", "May", "June",
  "July", "Aug", "Sep", "Oct", "Nov", "December"
];
      var maxds:string=d5.getFullYear() + "-" + (monthNames[d5.getMonth() ]) + "-" + d5.getDate();
     console.log(testDate,"date")
      forcast2.date=maxds;
      forcasting.date= maxds;  
     
      this.newforcast.push(JSON.parse(JSON.stringify(forcasting)));
      this.jsonObj.push(JSON.parse(JSON.stringify(forcasting)));
       this.week++;
    }
    
    for(var i=0;i<this.newforcast.length;i++)
 {
  this.n5.push(new Date(this.newforcast[i].date))
 }

    this.barChartPopulation();
    this.drawLineAndPath2();
  },error =>console.log('e',error));
  
}




@ViewChild('fileImportInput') fileImportInput: any;

fileChangeListener($event: any): void {

  const files = $event.srcElement.files;
  this.header = (this.header as unknown as string) === 'true' || this.header === true;

  this.ngxCsvParser.parse(files[0], { header: this.header, delimiter: ',' })
    .pipe().subscribe((result: Array<any>) => {
      console.log('Result', result);
      // this.csvRecords = result;
      this.arrayToJSONObject(result);
    }, (error: NgxCSVParserError) => {
      console.log('Error', error);
    });

}

arrayToJSONObject (csvRecords){
  //header
  var keys = csvRecords[0];

  //vacate keys from main array
  var newArr = csvRecords.slice(1, csvRecords.length);

  var formatted = [],
  data = newArr,
  cols = keys,
  l = cols.length;
  for (var i=0; i<data.length; i++) {
          var d = data[i],
                  o = {};
          for (var j=0; j<l; j++)
                  o[cols[j]] = d[j];
          formatted.push(o);
  }
 

  this.jsondata=JSON.stringify(formatted);
  this.jsonObj = JSON.parse(this.jsondata);
  this.jsonObj2=JSON.parse(this.jsondata);
 this.jsonObj.splice(0,this.jsonObj.length-this.n);
 this.jsonObj2.splice(0,this.jsonObj.length-this.n);
this.buildSvg();
this.addXandYAxis();
this.drawLineAndPath(); 
}



private buildSvg() {
  this.svg = d3.select('svg') // svg element from html
    .append('g')   // appends 'g' element for graph design
    .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
}

private addXandYAxis() {
  // range of data configuring
  this.x = d3Scale.scaleTime().range([5, this.width]);
  this.y = d3Scale.scaleLinear().range([this.height, 80]);
  // this.x.domain([d3Array.extent(this.jsonObj, (d) => new Date(d.date)),new Date()]);
  this.x.domain([new Date("12-25-2016"),new Date("2-24-2021")]);  
  // this.y.domain(d3Array.extent(this.jsonObj, (d) =>Number(d.TotFactoredVolume)));
  this.y.domain([0, 2500]);

  this.svg.append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .call(d3Axis.axisBottom(this.x));
  // Configure the Y Axis
  this.svg.append('g')
      .attr('class', 'axis axis--y')
      .call(d3Axis.axisLeft(this.y))
      
  // text label for the x axis
  this.svg.append("text")             
  .attr("transform",
        "translate(" + (this.width/2) + " ," + 
                       (this.height + this.margin.top + 20) + ")")
  .style("text-anchor", "middle")
  .style("fill", "black")
  .text("Date");

  // text label for the y axis
  this.svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - this.margin.left)
      .attr("x",0 - (this.height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .style("padding-right","10px")
      .style("fill", "black")
      
      .text("TotFactoredVolume"); 

     this. svg.append("text")
        .attr("x", 100)             
        .attr("y", 100)
        // .attr("text-anchor", "middle")  
        .style("fill","orange")
        .style("font-size", "15px")
        .style("font-weight","600") 

      //Labels
    
        .text("Past Data");

        this. svg.append("text")
        .attr("x", 200)             
        .attr("y", 100)
        .style("fill","steelblue")
        .style("font-size", "15px")
        .style("font-weight","600") 

        .text("Future Data");

        this.svg.append("rect")
        .attr("x", 80)
        .attr("y", 90)
        .attr("width", 10)
        .attr("height", 10)
        .attr("fill", "orange")

        this.svg.append("rect")
        .attr("x", 180)
        .attr("y", 90)
        .attr("width", 10)
        .attr("height", 10)
        .attr("fill", "steelblue")

        // this.svg.append('g')
        // .classed('labels-group', true)
        // .selectAll('text')
        // .data(this.jsonObj)
        // .enter()
        // .append('text')
        // .classed('label', true)
        // .text(function(d, i) {
        //   return d.TotFactoredVolume;
        // })
        // .attr('x',(d: any) => this.x(new Date(d.date) ))
        // .attr('y', (d: any) => this.y(Number(d.TotFactoredVolume) ))
        // .style('fill',"black")
        

        this.svg.selectAll("dot")    
        .data(this.jsonObj)         
    .enter().append("circle")                               
        .attr("r", 4)       
        .attr("cx",  (d: any) => this.x(new Date(d.date) ) )       
        .attr("cy", (d: any) => this.y(Number(d.TotFactoredVolume) )) 
        .data(this.jsonObj)
        .attr("y",function(d, i) {
            return d.TotFactoredVolume;
          })
        .style('fill',"orange")
        .append('title')
        .text(function(d, i) {
          return d.TotFactoredVolume;
        })
        // .on("mouseover", function(d) {      
        //     div.transition()        
        //         .duration(200)      
        //         .style("opacity", .9).style('fill',"black");      
        //     div.html( "<h1>"+this.y+"</h1>")  
        //         .style("left", "40px")     
        //         .style("top", "40px")
        //         .style("color","black");  
              
        //     })                  
        // .on("mouseout", function(d) {       
        //     div.transition()        
        //         .duration(500)      
        //         .style("opacity", 0);   
        // });
        
    


}
private drawLineAndPath() {
  this.line = d3Shape.line().curve(d3.curveNatural)
      .x( (d: any) => this.x(new Date(d.date) ))
      .y( (d: any) => this.y(Number(d.TotFactoredVolume) ))
       this.svg.append('path')
      .datum(this.jsonObj)
      .attr('class', 'line')
      .attr('d', this.line)
      .attr("stroke","orange")
      .style("stroke-width","3px")
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
   .append('title')
        .text(function(d, i) {
          return d.TotFactoredVolume;
        })
        
        
}
private drawLineAndPath2() {
  this.line2 = d3Shape.line().curve(d3.curveNatural)
      .x( (d: any) => this.x(new Date(d.date) ))
      .y( (d: any) => this.y(Number(d.TotFactoredVolume) ))
       this.svg.append('path')
      .datum(this.newforcast)
      .attr('class', 'line')
      .attr('d', this.line2)
      .attr("stroke","steelblue")
      .style("stroke-width","3px")
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .append('title')
      .text(function(d, i) {
        return d.TotFactoredVolume;
      })
      

     var values= this.svg.append('g')
      .classed('labels-group', true)
      .selectAll('text')
      .data(this.newforcast)
      .enter()
      .append('text')
      .classed('label', true)
      .text(function(d, i) {
        return d.TotFactoredVolume;
      })
      .attr('x',(d: any) => this.x(new Date(d.date) ))
      .attr('y', (d: any) => this.y(Number(d.TotFactoredVolume) ))
      .style('fill',"black")
      .append('text')
      .text(function(d, i) {
        return d.TotFactoredVolume;
      }).style("color","black")


     
}
barChartPopulation() {
  var data2= [
   {"date": new Date('10-05-2020'), "TotFactoredVolume": 900},
   {"date": new Date('10-12-2020'), "TotFactoredVolume": 1000},
   {"date": new Date('10-15-2020'), "TotFactoredVolume": 1600},
 ];
 var d6=[];
 var n6=[];
 for(var i=0;i<data2.length;i++)
 {
   d6.push(Number(data2[i].TotFactoredVolume))
  
 }
 
 for(var i=0;i<data2.length-1;i++)
 {
   n6.push(data2[i].date)
 }
 var d=[];
 for(var i=0;i<this.jsonObj.length;i++)
 {
   d.push(Number(this.jsonObj[i].TotFactoredVolume))
  
 }
 var d3=[];
 for(var i=0;i<this.newforcast.length;i++)
 {
   d3.push(Number(this.newforcast[i].TotFactoredVolume))
 }

 var n5=[]
 var maxd=new Date(this.jsonObj[this.jsonObj.length-1].date)
 var maxds:string=maxd.getFullYear() + "-" + (maxd.getMonth() + 1) + "-" + maxd.getDate();
 for(var i=0;i<this.jsonObj.length;i++)
 {
      n5.push(new Date(this.jsonObj[i].date))
 }
 


 console.log(n5,"kg")
 console.log( Date.UTC(2017, 0),"kg2")

 Highcharts.chart('barChart', {
   chart: {
     type: 'line'
   },
   
   title: {
     text: 'Historic World Population by Region'
   },
   xAxis: {
    categories:n5,
    type: 'datetime',
    plotLines: [{
      color: "orange",
      width: 2,
      value: this.jsonObj2.length-1
  }]
            // dateTimeLabelFormats: { // don't display the dummy year
            //     month: '%e. %b',
            //     year: '%b'}
},
  
   tooltip: {
     valueSuffix: ''
   },
 
   
   series: [{
     type: undefined,
     name: 'data',
    //  pointStart: Date.UTC(2016, 11,0,0,0),
    //  pointIntervalUnit: 'year',
     data: d,
     zoneAxis: 'x',
     zones: [{
      value:this.jsonObj2.length-1,
      color:"orange"
  },
  {
         
      value:this.jsonObj.length,
      color:"gray"
  }
 
]
   }
   
  ],
  
 });
}



}
