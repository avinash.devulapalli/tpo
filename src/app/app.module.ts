import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MaterialModule } from './material/material.module';
import{MatSelectModule} from'@angular/material/select';
import{ MatFormFieldModule } from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import{HttpClientModule} from '@angular/common/http'
import { NgxCsvParserModule } from 'ngx-csv-parser';
import { ChartModule } from 'angular-highcharts';
import { ForcastingComponent } from './forcasting/forcasting.component';
 



@NgModule({
  declarations: [
    AppComponent,
    ForcastingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    FormsModule,
    HttpClientModule,
    NgxCsvParserModule,
    ChartModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
